package com.akkastream.demo;

import akka.actor.ActorRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    ActorRunner actorRunner;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

    @Bean
    public ActorRef autoStartTestActor() throws Exception {
        System.out.println("======== Start Single Reduce ============ ");
	    actorRunner.runSingleReduce();

        Thread.sleep(5000);
        System.out.println("======== Fininshed Single Reduce ============ ");
        System.out.println("======== Wait 5 seconds start stream Reduce ============ ");
        Thread.sleep(5000);

        System.out.println("======== Start Stream Reduce ============ ");
        actorRunner.runStreamReduce();
        return null;
    }

}
