package com.akkastream.demo;

import akka.Done;
import akka.actor.ActorSystem;
import akka.dispatch.Mapper;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.*;
import org.springframework.stereotype.Component;
import scala.concurrent.impl.Promise;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 * Created by tong on 06/06/17.
 */
@Component
public class ActorRunner {

    private final ActorSystem actorSystem = ActorSystem.create("Stream-service-actor");


    public void runSingleReduce() {
        System.out.println("=============== Single run ");
        Mapper<SourceQueueWithComplete<SystemCodeTracking>, SourceQueueWithComplete<SystemCodeTracking>> sourceQueueMapper =
            new Mapper<SourceQueueWithComplete<SystemCodeTracking>, SourceQueueWithComplete<SystemCodeTracking>>() {

            @Override
            public SourceQueueWithComplete<SystemCodeTracking> apply(SourceQueueWithComplete<SystemCodeTracking> queue) {
                addTestData(queue);
                queue.complete();
                return queue;
            }

        };
        common().future().<SourceQueueWithComplete<SystemCodeTracking>>map(sourceQueueMapper, actorSystem.dispatcher());

    }

    public void runStreamReduce() {

        Mapper<SourceQueueWithComplete<SystemCodeTracking>, SourceQueueWithComplete<SystemCodeTracking>> sourceQueueMapper =
                new Mapper<SourceQueueWithComplete<SystemCodeTracking>, SourceQueueWithComplete<SystemCodeTracking>>() {

            @Override
            public SourceQueueWithComplete<SystemCodeTracking> apply(SourceQueueWithComplete<SystemCodeTracking> queue) {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        addTestData(queue);
                    }
                }, 0L, TimeUnit.SECONDS.toSeconds(10L));
                return queue;
            }

        };

        common().future().<SourceQueueWithComplete<SystemCodeTracking>>map(sourceQueueMapper, actorSystem.dispatcher());

    }

    private void addTestData(SourceQueueWithComplete<SystemCodeTracking> queue) {
        SystemCodeTracking s1 = new SystemCodeTracking(1L, "table1");
        SystemCodeTracking s2 = new SystemCodeTracking(2L, "table2");
        SystemCodeTracking s3 = new SystemCodeTracking(3L, "table3");
        SystemCodeTracking s4 = new SystemCodeTracking(4L, "table1");
        SystemCodeTracking s5 = new SystemCodeTracking(5L, "table1");
        SystemCodeTracking s6 = new SystemCodeTracking(6L, "table3");

        queue.offer(s1);
        queue.offer(s2);
        queue.offer(s3);
        queue.offer(s4);
        queue.offer(s5);
        queue.offer(s6);
    }

    private Promise<SourceQueueWithComplete<SystemCodeTracking>> common() {

        final Materializer materIalizer = ActorMaterializer.create(actorSystem);
        Promise<SourceQueueWithComplete<SystemCodeTracking>> promise = new Promise.DefaultPromise<SourceQueueWithComplete<SystemCodeTracking>>();

        Source<SystemCodeTracking, SourceQueueWithComplete<SystemCodeTracking>> source =
            Source.<SystemCodeTracking> queue(1000000, OverflowStrategy.fail())
            .mapMaterializedValue(queue -> {
                promise.trySuccess(queue);
                return queue;
        })
        .groupBy(20000, systemCodeTracking -> systemCodeTracking.getEntityName())
        .map(systemCodeTracking -> new Pair<>(systemCodeTracking.getEntityName(), systemCodeTracking))
        .reduce((prev, next) -> {
            next.second().setId(prev.second().getId() + next.second().getId());

            System.out.println("====== doing reduceing " + next.second().toString());
            return next;
        })
        .map(systemCodeTrackingPair -> systemCodeTrackingPair.second())
        .mergeSubstreams();

        Sink<SystemCodeTracking, CompletionStage<Done>> si7 = Sink.foreach(systemCodeTracking -> {
            System.out.println("====== Sink result " + systemCodeTracking.toString());

        });

        final RunnableGraph<SourceQueueWithComplete<SystemCodeTracking>> runner = source.toMat(si7, Keep.left());
        runner.run(materIalizer);
        return promise;

    }


    public static class SystemCodeTracking {

        public SystemCodeTracking(Long id, String entityName) {
            this.id = id;
            EntityName = entityName;
        }

        private Long id;
        private String EntityName;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getEntityName() {
            return EntityName;
        }

        public void setEntityName(String entityName) {
            EntityName = entityName;
        }

        @Override
        public String toString() {
            return "SystemCodeTracking{" +
                    "id=" + id +
                    ", EntityName='" + EntityName + '\'' +
                    '}';
        }
    }
}
