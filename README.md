## Demo Akka Stream reduce question for [stackoverflow question](https://stackoverflow.com/questions/44316740/akka-stream-sliding-window-to-control-reduce-emit-to-sink/44317588?noredirect=1#comment75647475_44317588)

##### How to run : 
```
./mvnw clean spring-boot:run

```

##### There are 2 test runing:
* single reduce (only push queue once then complete the queue) 
* stream reduce (stream reduce run by a timer that keep push data to a source queue)

##### Question:
The single reduce run is ok becasue once I call queue.complete() the source emit the result to sink and the sink print the result as what I want

```
======== Start Single Reduce ============ 
====== Sink result SystemCodeTracking{id=9, EntityName='table3'}
====== Sink result SystemCodeTracking{id=2, EntityName='table2'}
====== Sink result SystemCodeTracking{id=10, EntityName='table1'}

```

However for the stream reduce, since I never call queue.complete(), it will never emits the result to sink, and it will keep growing the reduce value.
I tried scan() but it is not what I want, becasue althrough it does send the data to sink but it still keep the old sum id and reduce on it.

##### What I want:
what I want is more like sliding window in [Apache Flink](https://flink.apache.org/news/2015/12/04/Introducing-windows.html) does. It can group x amount of data or by x amount of duration, it reducing the data and send the data to Sink,
then the next coming ones suppose be fresh. in my test case I would like to see following: 

```
1st round
====== Sink result SystemCodeTracking{id=9, EntityName='table3'}
====== Sink result SystemCodeTracking{id=2, EntityName='table2'}
====== Sink result SystemCodeTracking{id=10, EntityName='table1'}

2ed round
====== Sink result SystemCodeTracking{id=9, EntityName='table3'}
====== Sink result SystemCodeTracking{id=2, EntityName='table2'}
====== Sink result SystemCodeTracking{id=10, EntityName='table1'}

3ed round
====== Sink result SystemCodeTracking{id=9, EntityName='table3'}
====== Sink result SystemCodeTracking{id=2, EntityName='table2'}
====== Sink result SystemCodeTracking{id=10, EntityName='table1'}
......


```